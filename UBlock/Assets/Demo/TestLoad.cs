﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBlockly;
using UBlockly.Scriptable;
using UBlockly.UI;

public class TestLoad : MonoBehaviour
{
    [SerializeField]
    GameObject blockApi;
    [SerializeField]
    GameObject ublocklyGo;
    
    // Start is called before the first frame update
    void Start()
    {
        UBlockly.Util.UBlocklyFacade.GetIns().InitFrameWork(blockApi.GetComponent<IBlockApi>());
        UBlockly.Util.UBlocklyFacade.GetIns().LoadUBlockly(ublocklyGo);
        var files = System.IO.Directory.GetFiles("Assets/BlockAssets/GeneratorBlocks");
        List<NodeConfigAssist> list = new List<NodeConfigAssist>();
        for (int i = 0; i < files.Length; i++)
        {
            if (!files[i].EndsWith(".prefab"))
                continue;
            var go = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(files[i]);
            list.Add(go.GetComponent<NodeConfigAssist>());
        }
        UBlockly.Util.UBlocklyFacade.GetIns().InitBlocks(list);
    }
}
