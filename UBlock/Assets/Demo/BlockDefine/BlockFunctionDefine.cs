﻿using UnityEngine;
using System.ComponentModel;

namespace UBlockly.UI
{
    public enum BlockFunctionDefine : uint
    {
        #region  void Return Type
        [Description("")]
        vReturnFunction = 10000,
        [Description("藏起来")]
        vHideSelf = 10001,
        [Description("闪 现")]
        vShowSelf = 10002,
        [Description("闪 藏")]
        vActiveSelf = 10003,
        [Description("装 扮")]
        vChangeDress = 10004,
        [Description("装 扮")]
        vChangeDressUntilOver = 10005,
        [Description("行走")]
        vWalk = 10006,
        [Description("站立")]
        vStand = 10007,
        [Description("看自己")]
        vLookSelf = 10008,
        [Description("播放动作")]
        vPlayAnim = 10009,
        [Description("播动作直到完成")]
        vPlayAnimUntilOver = 10010,
        [Description("展示os")]
        vShowOS = 10011,

        [Description("如果#否则")]
        vIfElseCondition = 11001,

        #endregion

        #region  bool Return Type
        [Description("")]
        vBoolFunction = 20000,
        [Description("随机真假")]
        vBoolRandom = 20001,
        [Description("取反")]
        vAlwayNo = 20002,
        [Description("是否是偶数")]
        vIsEven = 20003,
        #endregion

        #region int Return Type
        [Description("")]
        vIntFunction = 30000,
        [Description("随机整数")]
        vRandomInt = 30001,
        [Description("获取年龄")]
        vGetAge = 30002,
        [Description("bool转int")]
        vBool2Int = 30003,
        [Description("IntTest")]
        vIntTest = 30004,
        [Description("漂亮吗")]
        vIsPretty = 30005,
        #endregion

        #region string Return Type
        [Description("")]
        sReturnFunction = 40000,
        [Description("站立动作")]
        sStandAnim = 40001,
        [Description("行走动作")]
        sWalkAnim = 40002,
        [Description("随机套装")]
        sDressSuitRandom = 40003,
        [Description("随机发型")]
        sDressHairRandom = 40004,
        [Description("随机外套")]
        sDressCoatRandom = 40005,
        [Description("随机裤子")]
        sDressPantRandom = 40006,
        [Description("随机动作")]
        sAinmRandom = 40007,
        [Description("测试字符")]
        sTestString = 40008,
        #endregion
    }
}
