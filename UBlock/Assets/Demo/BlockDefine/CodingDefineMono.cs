﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
//using ARMan;

namespace UBlockly.UI
{
    public partial class CodingDefineMono : MonoBehaviour,IBlockApi
    {
        #region void return

        [BlockFunction(BlockFunctionDefine.vHideSelf)]
        public void HideSelf(System.Action callback)
        {
            callback();
        }

        [BlockFunction(BlockFunctionDefine.vShowSelf)]
        public void ShowSelf(System.Action callback)
        {
            callback();
        }

        [BlockFunction(BlockFunctionDefine.vActiveSelf)]
        public void ActiveSelf(bool isActive, System.Action callback)
        {
            if (isActive)
                ShowSelf(callback);
            else
                HideSelf(callback);
            callback();
        }

        /// <summary>
        /// 装扮
        /// </summary>
        /// <param name="fittingId"></param>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vChangeDress)]
        public void ChangeDress(string fittingId, System.Action callback)
        {
            callback();
        }

        /// <summary>
        /// 装扮直到完成
        /// </summary>
        /// <param name="fittingId"></param>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vChangeDressUntilOver)]
        public void ChangeDressUntilOver(string fittingId, System.Action callback)
        {
            callback();
        }

        /// <summary>
        /// 走路
        /// </summary>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vWalk)]
        public void Walk(System.Action callback)
        {
            callback();
        }

        /// <summary>
        /// 站立
        /// </summary>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vStand)]
        public void Stand(System.Action callback)
        {
            callback();
        }

        /// <summary>
        /// 看自己
        /// </summary>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vLookSelf)]
        public void LookSelf(System.Action callback)
        {
            callback();
        }

        /// <summary>
        /// 播放动画
        /// </summary>
        /// <param name="anim"></param>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vPlayAnim)]
        public void PlayAnim(string anim, System.Action callback)
        {
            callback();
        }
        /// <summary>
        /// 播放动画直到完成
        /// </summary>
        /// <param name="aim"></param>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vPlayAnimUntilOver)]
        public void PlayAnimUntilOver(string aim, System.Action callback)
        { }
        /// <summary>
        /// 展示内心os
        /// </summary>
        /// <param name="os"></param>
        /// <param name="callback"></param>
        [BlockFunction(BlockFunctionDefine.vShowOS)]
        public void ShowOS(string os, System.Action callback)
        { }

        #endregion
        #region string return
        /// <summary>
        /// 站立动画
        /// </summary>
        /// <returns></returns>
        [BlockFunction(BlockFunctionDefine.sStandAnim)]
        public string StandAnim()
        {
            return "stand_girlidle";
        }
        /// <summary>
        /// 行走动画
        /// </summary>
        /// <returns></returns>
        [BlockFunction(BlockFunctionDefine.sWalkAnim)]
        public string WalkAnim()
        {
            return "stand_girlmove";
        }
        /// <summary>
        /// 随机套装
        /// </summary>
        /// <returns></returns>
        [BlockFunction(BlockFunctionDefine.sDressSuitRandom)]
        public string DressSuitRandom()
        {
            return "";
        }
        [BlockFunction(BlockFunctionDefine.sDressHairRandom)]
        public string DressHairRandom()
        {
            return "";
        }
        [BlockFunction(BlockFunctionDefine.sDressCoatRandom)]
        public string DressCoatRandom()
        {
            return "";
        }
        [BlockFunction(BlockFunctionDefine.sDressPantRandom)]
        public string DressPantRandom()
        {
            return "";
        }
        [BlockFunction(BlockFunctionDefine.sAinmRandom)]
        public string AinmRandom()
        {
            return "";
        }
        [BlockFunction(BlockFunctionDefine.sTestString)]
        public string TestString(string test)
        {
            return test;
        }
        #endregion
        #region bool return
        [BlockFunction(BlockFunctionDefine.vBoolRandom)]
        public bool BoolRandom()
        {
            var r = new System.Random().Next();
            return r % 2 == 0 ? true : false;
        }
        [BlockFunction(BlockFunctionDefine.vAlwayNo)]
        public bool AlwayNo(bool value)
        {
            return !value;
        }
        [BlockFunction(BlockFunctionDefine.vIsEven)]
        public bool IsEven(int value)
        {
            return value % 2 == 0;
        }
        [BlockFunction(BlockFunctionDefine.vIsPretty)]
        public bool IsPretty(string name)
        {
            return name == "xiongsonglin";
        }
        #endregion
        #region int return
        [BlockFunction(BlockFunctionDefine.vRandomInt)]
        public int RandomInt(int max)
        {
            return new System.Random().Next(max);
        }
        [BlockFunction(BlockFunctionDefine.vGetAge)]
        public int GetAge()
        {
            return 0;
        }
        [BlockFunction(BlockFunctionDefine.vBool2Int)]
        public int Bool2Int(bool value)
        {
            return value ? 1 : 0;
        }
        [BlockFunction(BlockFunctionDefine.vIntTest)]
        public int IntTest(string test)
        {
            return 0;
        }
        #endregion

        #region  specail test

        #endregion

    }
    partial class CodingDefineMono
    {

        //void Start()
        //{
        //    UBlockly.Scriptable.ILTranslate.LoadApi(this);
        //}
    }
}
