﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSize
{
    //常规类型node
    public static readonly int PackParamBorder = 4;
    public static readonly int PackParamRightSpace = 10;
    public static readonly int PackParamUpSpace = 10;
    public static readonly int PackParamDownSpace = 20;
    public static readonly int PackParamLen_Int = 80;
    public static readonly int PackParamLen_Bool = 80;
    public static readonly int PackParamLen_String = 150;
    public static readonly int PackParamHeight = 100;
    public static readonly int VoidNodeHeight = 110;
    public static readonly int NVoidNodeHeight = 86;
    public static readonly int DescLeftBorder = 10;
    public static readonly int DescRightBorder = 20;
    public static readonly int DescHeight = 60;
    public static readonly int DescFontSize = 26;
    public static readonly int InputComponentFontSize = 22;
    public static readonly int NodeVerticalSpace = 4;

    //条件类型node
    public static readonly int ConditionLeftBoard = 40;
    public static readonly int FirstConditionHeight = 110;
    public static readonly int SecondConditionHeight = 80;
    public static readonly int ThridConditionHeight = 60;
    public static readonly int ConditionSpaceHeight = 110;

    //color
    public static readonly Color PackParam_BoolColor = Color.yellow;
    public static readonly Color PackParam_IntColor = Color.yellow;
    public static readonly Color PackParam_StringColor = Color.yellow;
    public static readonly Color VoidTypeNodeColor = new Color32(104, 104, 171, 255);
    public static readonly Color BoolTypeNodeColor = Color.cyan;
    public static readonly Color IntTypeNodeColor = Color.green;
    public static readonly Color StringTypeNodeColor = Color.white;

    //spirte
    public static readonly string VoidReturnNodeSpriteName = "voidReturnNodeSprite";
    public static readonly string BoolReturnNodeSpritName = "boolReturnNodeSprite";
    public static readonly string IntReturnNodeSpritName = "intReturnNodeSprite";
    public static readonly string StringReturnNodeSpritName = "stringReturnNodeSprite";
    public static readonly string ConditionNodeSprite1 = "voidReturnNodeSprite";
    public static readonly string ConditionNodeSprite2 = "voidReturnNodeSprite";
    public static readonly string ConditionNodeSprite3 = "voidReturnNodeSprite";

    public static readonly Vector2 SlotRect = new Vector2(140, 50);
}
