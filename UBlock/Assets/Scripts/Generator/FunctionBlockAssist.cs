﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBlockly.Util;
using UBlockly.Scriptable;
public struct FunctionBlockAssist
{
    public string FunctionDesc;
    public string FunctionDefineStr;
    public KeyValuePair<string, object>[] defaultParams;

    public string GetFuncName()
    {
        return UTool.ParseFuncName(FunctionDefineStr);
    }
    public ILFuncType GetFuncType()
    {
        return UTool.ParseFuncType(FunctionDefineStr);
    }
}
