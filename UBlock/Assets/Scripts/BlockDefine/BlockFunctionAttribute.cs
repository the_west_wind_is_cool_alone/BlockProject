﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.UI
{
    public struct BlockDefaultParam
    {
        public string DisplayValue;
        public object TrueValue;
    }

    public class BlockFunctionAttribute : System.Attribute
    {
        private uint classify;
        private uint id;
        private KeyValuePair<string, object>[] defaultParams;
        public uint Id => id;
        public uint Classify => classify;
        public KeyValuePair<string, object>[] DefaultParams => defaultParams;
        public BlockFunctionAttribute(BlockFunctionDefine blockFuncId, uint classify = 0) : base()
        {
            this.id = (uint)blockFuncId;
            this.classify = classify;
        }
        
    }
}
