﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBlockly.UI;
using UBlockly.Compotent;
using UBlockly.Scriptable;

[RequireComponent(typeof(BlockAnchor))]
public abstract class BlockObject : MonoBehaviour
{
    public BlockEventHandler EventHandler;
    public LinkedListNode<Node> Node { get; private set; }
    public Vector3 Position => transform.position;
    public Vector3 LocalPosition => transform.localPosition;
    public Transform Parent => transform.parent;
    public BlockAnchor BlockAnchor => GetComponent<BlockAnchor>();
    public BlockType BlockType { get; private set; }
    [SerializeField]
    protected List<BlockAnchor> blist;

    private void Awake()
    {
        EventHandler = gameObject.AddCompontentIfNoExist<BlockEventHandler>();
        BlockAnchor.RectTransform.anchorMin = new Vector2(0, 1);
        BlockAnchor.RectTransform.anchorMax = new Vector2(0, 1);
        BlockAnchor.RectTransform.pivot = new Vector2(0, 1);
        blist = new List<BlockAnchor>(3);
        if (transform.childCount != 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var anchor = transform.GetChild(i).GetComponent<BlockAnchor>();
                if(anchor != null)
                    blist.Add(anchor);
            }
        }
    }

    public void BindNode(Node node)
    {
        Node = new LinkedListNode<Node>(node);
        BlockType = node.Scriptable.ResultType == UBlockly.Scriptable.ILFunResult.VoidR ? BlockType.SlotType : BlockType.PackType;
        if (BlockType == BlockType.SlotType)
        {
            ULinkedList<Node> list = new ULinkedList<Node>();
            list.MuteNotify();
            list.AddFirst(Node);
            list.OpenNoify();
        }
    }

    public void AttacthToBlock(BlockAnchor parentBlock)
    {
        BlockAnchor.AttachToBlock(parentBlock);
        transform.localPosition += new Vector3(0,0,1);
    }

    public void UnAttach(Transform parent)
    {
        BlockAnchor.SetParent(parent);
        transform.localPosition -= new Vector3(0,0,1);
    }

    public void ReSize()
    {
        float width = 0;
        float height = 0;
        foreach (var b in blist)
        {
            if (b.name == "PackInfo")
            {
                var pnode = b.transform.GetChild(b.transform.childCount - 1).GetComponent<BlockAnchor>();
                b.SetSize(new Vector2(BSize.PackParamBorder * 2 + pnode.GetSize().x, pnode.GetSize().y));
                pnode.RectTransform.anchoredPosition = new Vector2(BSize.PackParamBorder,0);
            }
            width += b.GetSize().x;
            if (b.name == "Desc")
                width += (BSize.DescLeftBorder + BSize.DescRightBorder);
            if (b.name == "PackInfo")
            {
                width += BSize.PackParamRightSpace;
            }

            height = b.GetSize().y > height ? b.GetSize().y : height;
        }

        height += Node.Value.Scriptable.ResultType == ILFunResult.VoidR
            ? (BSize.PackParamBorder * 2 + BSize.PackParamUpSpace + BSize.PackParamDownSpace)
            : (BSize.PackParamBorder * 2 + BSize.PackParamUpSpace * 2);
        BlockAnchor.SetSize(new Vector2(width, height));
    }

    public Vector2 GetSize()
    {
        return BlockAnchor.GetSize();
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }

    public Transform Find(string name)
    {
        return transform.Find(name);
    }
}
