﻿using System;
using UBlockly.UI;
using UnityEngine;
using UBlockly.Scriptable;
using UBlockly.Scriptable.Function;

namespace UBlockly.Util
{

    public sealed class NodeFactory
    {
        public static Node CreateNode(NodeConfigAssist assit)
        {
            GameObject go = GameObject.Instantiate(assit.gameObject);
            assit = go.GetComponent<NodeConfigAssist>();
            var block = go.GetComponent<BlockObject>();
            var function = FunctionFactory.CreateILFunc(assit.FuncType);        
            NodeScriptable x = new NodeScriptable(function);
            x.BindFuncName(assit.FunctionName);
            Node node = Activator.CreateInstance(ILTranslate.GetNodeType(x.FuncType), block,x) as Node;
            return node;
        }
    }

    public static class FunctionFactory
    {
        public static ILFunc CreateILFunc(ILFuncType type)
        {
            ILFunc func = null;
            switch (type)
            {
                case ILFuncType.BoolBool:
                    func = new BoolBoolILFunc();
                    break;
                case ILFuncType.BoolInt:
                    func = new BoolIntILFunc();
                    break;
                case ILFuncType.BoolString:
                    func = new BoolStringILFunc();
                    break;
                case ILFuncType.BoolVoid:
                    func = new BoolVoidILFunc();
                    break;
                case ILFuncType.IntBool:
                    func = new IntBoolILFunc();
                    break;
                case ILFuncType.IntInt:
                    func = new IntIntILFunc();
                    break;
                case ILFuncType.IntString:
                    func = new IntStringILFunc();
                    break;
                case ILFuncType.IntVoid:
                    func = new IntVoidILFunc();
                    break;
                case ILFuncType.StringBool:
                    func = new StringBoolILFunc();
                    break;
                case ILFuncType.StringInt:
                    func = new StringIntILFunc();
                    break;
                case ILFuncType.StringString:
                    func = new StringStringILFunc();
                    break;
                case ILFuncType.StringVoid:
                    func = new StringVoidILFunc();
                    break;
                case ILFuncType.VoidBool:
                    func = new VoidBoolILFunc();
                    break;
                case ILFuncType.VoidInt:
                    func = new VoidIntILFunc();
                    break;
                case ILFuncType.VoidString:
                    func = new VoidStringILFunc();
                    break;
                case ILFuncType.VoidVoid:
                    func = new VoidVoidILFunc();
                    break;
                case ILFuncType.IfCondition:

                    break;
                case ILFuncType.IfElseCondition:
                    func = new IfElseConditionILFunc();
                    break;
                default:
                    break;
            }
            return func;
        }
        public static T CreateILFunc<T>(ILFuncType type) where T : ILFunc
        {
            return CreateILFunc(type) as T;
        }
    }
}
