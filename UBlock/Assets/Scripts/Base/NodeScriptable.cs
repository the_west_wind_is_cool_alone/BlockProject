﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Scriptable
{

    public sealed class NodeScriptable
    {
        private ILFunc Func;
        public ILFuncType FuncType => Func.FuncType;
        public ILFunResult ResultType => Func.ResultType;
        public ILFunResult ParamType => Func.ParamType;
        public NodeScriptable(ILFunc fun)
        {
            this.Func = fun;
        }

        public void BindFuncName(string funcname, System.Func<object> DefaultParamGetter = null)
        {
            this.Func.Init(funcname, DefaultParamGetter);
        }

        public void InitParam(ILFunc param)
        {
            Func.InitParam(param);
        }

        public ILFunc GetFunc()
        {
            return Func;
        }

        public string ToILDefineFormat()
        {
            if (Func != null && !string.IsNullOrEmpty(Func.FunName))
                return string.Format("{0} {1}((2) {3})", ResultType, Func.FunName, ParamType, "param");
            return "";
        }

        public string ToILCallFormat(object param = null)
        {
            if (Func != null && !string.IsNullOrEmpty(Func.FunName))
            {
                if (param != null)
                    return string.Format("{0}({1})", Func.FunName, param);
                else
                    return string.Format("{0}()", Func.FunName);
            }
            return "";
        }
    }
}