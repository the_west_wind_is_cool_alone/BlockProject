﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UBlockly.Scriptable
{
    /// <summary>
    /// 函数返回值类型
    /// </summary>
    public enum ILFunResult
    {
        IntR = 0,
        StringR = 1,
        VoidR = 2,
        BoolR = 3,
        None,
    }
    /// <summary>
    /// 函数块类型
    /// </summary>
    public enum ILFuncType : uint
    {
        NoDefine = 0,

        //常规类型函数定义 1xx
        VoidVoid = 100,
        VoidBool = 101,
        VoidInt = 102,
        VoidString = 103,
        BoolVoid = 104,
        BoolBool = 105,
        BoolInt = 106,
        BoolString = 107,
        IntVoid = 108,
        IntBool = 109,
        IntInt = 110,
        IntString = 111,
        StringVoid = 112,
        StringBool = 113,
        StringInt = 114,
        StringString = 115,

        //条件类型函数定义 3xx
        IfCondition = 300,
        IfElseCondition = 301,
    }
}

