﻿using UBlockly.UI;
using UBlockly.Scriptable;
using System.Collections.Generic;
using System;

namespace UBlockly.Scriptable.Function
{
    #region Special 类型，函数内部由Function构成
    ///// <summary>
    ///// if else语句块
    ///// </summary>
    //public class Specail_VoidBoolILFunc : VoidILFunc
    //{
    //    public override ILFuncType FuncType => ILFuncType.Special;
    //    public override ILFunResult ParamType => ILFunResult.BoolR;
    //    public BoolILFunc FunParam;
    //    List<VoidILFunc> voidILs1 = new List<VoidILFunc>();
    //    List<VoidILFunc> voidILs2 = new List<VoidILFunc>();
    //    public void Init(string nm, ILFunc param, List<ILFunc> content1, List<ILFunc> content2)
    //    {
    //        FunName = nm;
    //        FunParam = param as BoolILFunc;
    //        for (int i = 0; i < content1.Count; i++)
    //        {
    //            voidILs1.Add(content1[i] as VoidILFunc);
    //        }
    //        for (int i = 0; i < content2.Count; i++)
    //        {
    //            voidILs2.Add(content2[i] as VoidILFunc);
    //        }
    //    }
    //    public override void Excute(System.Action endCallback)
    //    {
    //        if (FunParam.Excute())
    //        {
    //            foreach (var fun in voidILs1)
    //            {
    //                fun.Excute(endCallback);
    //            }
    //        }
    //        else
    //        {
    //            foreach (var fun in voidILs2)
    //            {
    //                fun.Excute(endCallback);
    //            }
    //        }
    //    }
    //}

    public class IfElseConditionILFunc : VoidILFunc
    {
        public override ILFuncType FuncType => ILFuncType.IfElseCondition;
        public override ILFunResult ParamType => ILFunResult.BoolR;
        public BoolILFunc FunParam;
        List<VoidILFunc> voidILs1 = new List<VoidILFunc>();
        List<VoidILFunc> voidILs2 = new List<VoidILFunc>();

        public override void Excute(Action endCallback)
        {
            if (FunParam.Excute())
            {

                endCallback();
            }
            else
            {
                endCallback();
            }
        }
        void Continue()
        { }
    }
    #endregion
    #region  return void 类型
    public sealed class VoidVoidILFunc : VoidILFunc
    {
        public override ILFuncType FuncType => ILFuncType.VoidVoid;
        public override ILFunResult ParamType => ILFunResult.VoidR;
        public override void Excute(System.Action endCallback)
        {
            ILTranslate.ExF(FunName, endCallback);
        }
    }
    public sealed class VoidBoolILFunc : VoidILFunc
    {
        public override ILFuncType FuncType => ILFuncType.VoidBool;
        public override ILFunResult ParamType => ILFunResult.BoolR;
        BoolILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as BoolILFunc;
        }

        public override void Excute(System.Action endCallback)
        {
            ILTranslate.ExF(FunName,endCallback, param.Excute());
        }
    }
    public sealed class VoidIntILFunc : VoidILFunc
    {
        public override ILFuncType FuncType => ILFuncType.VoidInt;
        public override ILFunResult ParamType => ILFunResult.IntR;
        IntILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as IntILFunc;
        }

        public override void Excute(System.Action endCallback)
        {
            ILTranslate.ExF(FunName, param.Excute(), endCallback);
        }
    }
    public sealed class VoidStringILFunc : VoidILFunc
    {
        public override ILFuncType FuncType => ILFuncType.VoidString;
        public override ILFunResult ParamType => ILFunResult.StringR;
        StringILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as StringILFunc;
        }

        public override void Excute(System.Action endCallback)
        {
            ILTranslate.ExF(FunName, endCallback, param.Excute());
        }
    }
    #endregion
    #region return bool 类型
    public sealed class BoolVoidILFunc : BoolILFunc
    {
        public override ILFuncType FuncType => ILFuncType.BoolVoid;
        public override ILFunResult ParamType => ILFunResult.VoidR;
        public override bool Excute()
        {
            return ILTranslate.BExF(FunName,DefaultParamGetter?.Invoke());
        }
    }
    public sealed class BoolBoolILFunc : BoolILFunc
    {
        public override ILFuncType FuncType => ILFuncType.BoolBool;
        public override ILFunResult ParamType => ILFunResult.BoolR;
        BoolILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as BoolILFunc;
        }

        public override bool Excute()
        {
            return ILTranslate.BExF(FunName, param.Excute());
        }
    }
    public sealed class BoolIntILFunc : BoolILFunc
    {
        public override ILFuncType FuncType => ILFuncType.BoolInt;
        public override ILFunResult ParamType => ILFunResult.IntR;
        IntILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as IntILFunc;
        }

        public override bool Excute()
        {
            return ILTranslate.BExF(FunName, param.Excute());
        }
    }
    public sealed class BoolStringILFunc : BoolILFunc
    {
        public override ILFuncType FuncType => ILFuncType.BoolString;
        public override ILFunResult ParamType => ILFunResult.StringR;
        StringILFunc param;
        public override void InitParam(ILFunc func)
        {
            param = func as StringILFunc;
        }

        public override bool Excute()
        {
            return ILTranslate.BExF(FunName, param.Excute());
        }
    }
    #endregion
    #region return int 类型
    public sealed class IntVoidILFunc : IntILFunc
    {
        public override ILFuncType FuncType => ILFuncType.IntVoid;
        public override ILFunResult ParamType => ILFunResult.VoidR;
        public override int Excute()
        {
            return ILTranslate.IExF(FunName,DefaultParamGetter?.Invoke());
        }
    }
    public sealed class IntBoolILFunc : IntILFunc
    {
        public override ILFuncType FuncType => ILFuncType.IntBool;
        public override ILFunResult ParamType => ILFunResult.BoolR;
        BoolILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as BoolILFunc;
        }
        public override int Excute()
        {
            return ILTranslate.IExF(FunName, FuncParam.Excute());
        }
    }
    public sealed class IntIntILFunc : IntILFunc
    {
        public override ILFuncType FuncType => ILFuncType.IntInt;
        public override ILFunResult ParamType => ILFunResult.IntR;
        IntILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as IntILFunc;
        }
        public override int Excute()
        {
            return ILTranslate.IExF(FunName, FuncParam.Excute());
        }
    }
    public sealed class IntStringILFunc : IntILFunc
    {
        public override ILFuncType FuncType => ILFuncType.IntString;
        public override ILFunResult ParamType => ILFunResult.StringR;
        StringILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as StringILFunc;
        }
        public override int Excute()
        {
            return ILTranslate.IExF(FunName, FuncParam.Excute());
        }
    }
    #endregion
    #region return string 类型
    public sealed class StringVoidILFunc : StringILFunc
    {
        public override ILFuncType FuncType => ILFuncType.StringVoid;
        public override ILFunResult ParamType => ILFunResult.VoidR;
        public override string Excute()
        {
            return ILTranslate.SExF(FunName, DefaultParamGetter?.Invoke());
        }
    }
    public sealed class StringBoolILFunc : StringILFunc
    {
        public override ILFuncType FuncType => ILFuncType.StringBool;
        public override ILFunResult ParamType => ILFunResult.BoolR;
        BoolILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as BoolILFunc;
        }
        public override string Excute()
        {
            return ILTranslate.SExF(FunName, FuncParam.Excute());
        }
    }
    public sealed class StringIntILFunc : StringILFunc
    {
        public override ILFuncType FuncType => ILFuncType.StringInt;
        public override ILFunResult ParamType => ILFunResult.IntR;
        IntILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as IntILFunc;
        }
        public override string Excute()
        {
            return ILTranslate.SExF(FunName, FuncParam.Excute());
        }
    }
    public sealed class StringStringILFunc : StringILFunc
    {
        public override ILFuncType FuncType => ILFuncType.StringString;
        public override ILFunResult ParamType => ILFunResult.StringR;
        StringILFunc FuncParam;
        public override void InitParam(ILFunc func)
        {
            FuncParam = func as StringILFunc;
        }
        public override string Excute()
        {
            return ILTranslate.SExF(FunName, FuncParam.Excute());
        }
    }
    #endregion
}
