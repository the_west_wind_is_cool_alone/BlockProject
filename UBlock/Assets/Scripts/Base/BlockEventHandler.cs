﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

/// <summary>
/// 
/// </summary>
public class BlockEventHandler : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    bool isDragging = false;
    public BlockObject Block { get; private set; }
    public Action<PointerEventData,BlockObject> OnClickDel;
    public Action<PointerEventData,BlockObject> OnBeginDragDel;
    public Action<PointerEventData,BlockObject> OnDraggingDel;
    public Action<PointerEventData,BlockObject> OnDropDel;

    void Awake()
    {
        Block = GetComponent<BlockObject>();
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (isDragging)   //屏蔽拖动时触发的点击
            return;
        OnClickDel?.Invoke(eventData,Block);
    }
    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.pointerId == -2) //右键不能拖动
            return;
        isDragging = true;
        OnBeginDragDel?.Invoke(eventData, Block);
    }
    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        OnDropDel?.Invoke(eventData, Block);
    }
    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (eventData.pointerId == -2)
            return;
        OnDraggingDel?.Invoke(eventData, Block);
    }

    public static void DragBlock(PointerEventData data,BlockObject block)
    {
        var rt = block.BlockAnchor.RectTransform;

        Vector3 globalMousePos;
//            Vector2 localpos;
//            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rt, data.position, data.pressEventCamera, out localpos))
//            {
//                rt.localPosition = data.position + localpos;
//            }
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rt, data.position, data.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
        }
    }
}
