﻿using UnityEngine;

namespace UBlockly.UI
{
    public sealed class NSlotPair
    {
        private long id;
        private RectTransform upRect;
        private RectTransform downRect;
        private int index;
        public RectTransform ARect => upRect;
        public RectTransform BRect => downRect;
        public long NodeId => id;
        public NSlotPair(long nodeId, Transform up, Transform down,int dex = 0)
        {
            id = nodeId;
            index = dex;
            upRect = downRect = null;
            if (up != null && up.GetComponent<RectTransform>() != null)
                upRect = up.GetComponent<RectTransform>();
            if (down != null && down.GetComponent<RectTransform>() != null)
                downRect = down.GetComponent<RectTransform>();
        }
        public void ActiveARect(bool enable)
        {
            if (upRect != null)
            {
                upRect.gameObject.SetActive(enable);
                Debug.Log($"{id}上边检测点被{enable}");
            }
        }
        public void ActiveBRect(bool enable)
        {
            if (downRect != null)
            {
                downRect.gameObject.SetActive(enable);
                Debug.Log($"{id}下边检测点被{enable}");
            }
        }
        public void MuteAllRect(bool mute = true)
        {
            ActiveARect(!mute);
            ActiveBRect(!mute);
        }

        public int IsSlotPossible(NSlotPair anther)
        {
            if (anther != null)
            {
                if (ARect != null && anther.BRect != null && ARect.gameObject.activeSelf && anther.BRect.gameObject.activeSelf)
                {
                    if (UBtil.IsRecttransAcross(ARect, anther.BRect))
                        return 2;
                }
                if (BRect != null && anther.ARect != null && BRect.gameObject.activeSelf && anther.ARect.gameObject.activeSelf)
                {
                    if (UBtil.IsRecttransAcross(BRect, anther.ARect))
                        return 1;
                }
            }
            return 0;
        }
    }
}
