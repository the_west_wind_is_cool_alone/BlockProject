﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UBlockly.Scriptable
{
    public abstract class ILFunc
    {
        public string FunName;
        public System.Func<object> DefaultParamGetter;
        public abstract ILFunResult ResultType { get; }
        public abstract ILFunResult ParamType { get; }
        public abstract ILFuncType FuncType { get; }
        public void Init(string functionName,System.Func<object> defaultParamGetter)
        {
            FunName = functionName;
            DefaultParamGetter = defaultParamGetter;
        }
        public virtual void InitParam(ILFunc func)
        { }
        public void TryExcute(System.Action ExcuteOver = null)
        {
            if (this is VoidILFunc)
            {
                ((VoidILFunc)(this)).Excute(ExcuteOver);
            }
            else if (this is BoolILFunc)
            {
                ((BoolILFunc)(this)).Excute();
            }
            else if (this is IntILFunc)
            {
                ((IntILFunc)(this)).Excute();
            }
            else if (this is StringILFunc)
            {
                ((StringILFunc)(this)).Excute();
            }
        }
        //public abstract object TryExcute();
        //public abstract ILFunc TryGetParam();
    }

    public abstract class VoidILFunc : ILFunc
    {
        public sealed override ILFunResult ResultType => ILFunResult.VoidR;
        public abstract void Excute(System.Action endCallback);
    }

    public abstract class StringILFunc : ILFunc
    {
        public override ILFunResult ResultType => ILFunResult.StringR;
        public abstract string Excute();
    }

    public abstract class IntILFunc : ILFunc
    {
        public override ILFunResult ResultType => ILFunResult.IntR;
        public abstract int Excute();
    }

    public abstract class BoolILFunc : ILFunc
    {
        public override ILFunResult ResultType => ILFunResult.BoolR;
        public abstract bool Excute();
    }
}
