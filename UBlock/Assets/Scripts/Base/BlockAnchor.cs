﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class BlockAnchor : MonoBehaviour
{
    public RectTransform RectTransform => GetComponent<RectTransform>();

    public void AttachToBlock(BlockAnchor parentBlock)
    {
        SetParent(parentBlock.transform);
    }
    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
        RectTransform.anchorMin = new Vector2(0, 0.5f);
        RectTransform.anchorMax = new Vector2(0, 0.5f);
        RectTransform.pivot = new Vector2(0, 0.5f);
    }
    public Vector2 GetSize()
    {
        return RectTransform.sizeDelta;
    }
    /// <summary>
    /// 设置尺寸
    /// </summary>
    /// <param name="size"></param>
    public void SetSize(Vector2 size)
    {
        RectTransform.sizeDelta = size;
    }
    /// <summary>
    /// 设置边界
    /// </summary>
    /// <param name="border"></param>
    public void SetBorder(Vector2 border)
    {
        var panchor = transform.parent.GetComponent<BlockAnchor>();
        if (panchor == null)
            return;
        RectTransform.anchoredPosition += border;
        SetSize(GetSize() - 2 * border);
    }
}
