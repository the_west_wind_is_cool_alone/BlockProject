﻿using System.Collections;
using System.Collections.Generic;
using UBlockly.Scriptable;
using UnityEngine;

public sealed class PackTypeBlock : BlockObject
{
    public ILFunResult PackType => Node.Value.Scriptable.ResultType;
}
