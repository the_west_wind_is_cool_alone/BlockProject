﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameUpdateMgr : MonoBehaviour
{
    List<IFrameUpdate> _list = new List<IFrameUpdate>(100);

    public static FrameUpdateMgr Instance;

    private void Awake()
    {
        this.transform.localScale = new Vector3(1, (Screen.height / 1200f) / (Screen.width / 1920f), 1);
        Instance = this;
    }

    public void RegistFrame(IFrameUpdate iframe)
    {
        _list.Add(iframe);
    }

    public void UnRegistFrame(IFrameUpdate iframe)
    {
        _list.Remove(iframe);
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < _list.Count; i++)
            _list[i].UpdateDo(Time.deltaTime);
    }
}
