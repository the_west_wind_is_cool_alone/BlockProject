﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBlockly.UI;
using UBlockly.Compotent;

namespace UBlockly.Sys
{

    public partial class BlockSystem : CoreRunningSystem
    {
        public List<SlotTypeBlock> slotTypeBlockList = new List<SlotTypeBlock>();
        public List<NodePacker> packerList = new List<NodePacker>();

        protected override void OnInit(params object[] args)
        {
            base.OnInit(args);
            rootTransformForNode =
                Util.UBlocklyFacade.GetIns().Find("PaintingCavas/WorkSpace/ViewPort/Content");
        }

        public void AddBlock(BlockObject block)
        {
            block.transform.SetParent(rootTransformForNode);
            IBranchNode bnode = block.Node.Value as IBranchNode;
            if (bnode == null) //非分支node
            {
                if (block.BlockType == BlockType.SlotType)
                    slotTypeBlockList.Add(block as SlotTypeBlock);
                if(block.Node.Value.NodePacker != null)
                    packerList.Add(block.Node.Value.NodePacker);
            }
            else
            {
                for (int i = 0; i < bnode.BranchCount; i++)
                {
                    AddBlock(bnode.GetBranchRoot(i).Block);
                }
            }
        }
        public void RemoveBlock(BlockObject block)
        {
            IBranchNode bnode = block.Node.Value as IBranchNode;
            if (bnode == null) //非分支node
            {
                if (block.BlockType == BlockType.SlotType)
                    slotTypeBlockList.Remove(block as SlotTypeBlock);
                if (block.Node.Value.NodePacker != null)
                    packerList.Remove(block.Node.Value.NodePacker);
            }
            else
            {
                for (int i = 0; i < bnode.BranchCount; i++)
                {
                    RemoveBlock(bnode.GetBranchRoot(i).Block);
                }
            }
        }
        public void DoPack(PackTypeBlock target, NodePacker bePacked, int index = 0)
        {
            Debug.Log($"执行物理嵌套关系{target == null} - {bePacked == null}");
            var paramtrans = bePacked.GetParam(index).Block?.transform;
            if (paramtrans != null)
            {
                bePacked.UnPackNode(rootTransformForNode);
            }

            bePacked.PackNode(target.Node.Value);                //设置逻辑嵌套关系
        }
        public void DoSlot(BlockObject target, BlockObject beSloted,int slotKeyId)
        {
            Debug.Log($"slot成功");
            //不用去区分分支
            ULinkedList<Node> list = target.Node.List as ULinkedList<Node>;
            if (list == null)
            {
                list = new ULinkedList<Node>();
                list.MuteNotify();
                list.AddFirst(target.Node);
            }
            beSloted.Node.Insert(list);
        }
    }

    partial class BlockSystem
    {
        List<BlockObject> theWholeBlocks = new List<BlockObject>();
        private Transform rootTransformForNode;
    }
}
