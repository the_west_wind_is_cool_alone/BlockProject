﻿namespace UBlockly
{
    using Util;
    public abstract class ISystem
    {
        public TSys GetSys<TSys>() where TSys : ISystem
        {
            return UBlocklyFacade.GetIns().GetSys<TSys>();
        }

        protected virtual void OnInit(params object[] args)
        {
            UnityEngine.Debug.Log("初始化："+GetType().Name);
        }
    }
}
