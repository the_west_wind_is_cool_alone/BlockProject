﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UBlockly.UI;

namespace UBlockly.Sys
{

    public partial class NodeSystem : CoreRunningSystem
    {
        public List<Node> nodeList = new List<Node>(300);
        public void AddNode(Node node)
        {
            nodeList.Add(node);
            blockSys.AddBlock(node.Block);
            node.Block.EventHandler.OnClickDel  +=  ClickNode;
            node.Block.EventHandler.OnDraggingDel += DragNode;
            node.Block.EventHandler.OnBeginDragDel += BeginDragNode;
            node.Block.EventHandler.OnDropDel += EndDragNode;

            //physicSys.AddNode(node);
            //combineSys.AddNode(node);
        }

        public void RemoveNode(Node node)
        {
            node.Block.EventHandler.OnClickDel  -=  ClickNode;
            node.Block.EventHandler.OnDraggingDel -= DragNode;
            node.Block.EventHandler.OnBeginDragDel -= BeginDragNode;
            node.Block.EventHandler.OnDropDel -= EndDragNode;
            blockSys.RemoveBlock(node.Block);
            nodeList.Remove(node);
        }

        public Node FindNode(long id)
        {
            return nodeList.Find(x => x.Id == id);
        }
        
    }

    partial class NodeSystem
    {
        void BeginDragNode(PointerEventData data,BlockObject block)
        {
            paintingSystem.HandlerBlock(block);
        }
        void DragNode(PointerEventData data,BlockObject block)
        {
            BlockEventHandler.DragBlock(data,block);
            
        }
        void EndDragNode(PointerEventData data,BlockObject block)
        {
            paintingSystem.HandlerOver(block);
        }
        void ClickNode(PointerEventData data,BlockObject block)
        {
            
        }
    }
}
