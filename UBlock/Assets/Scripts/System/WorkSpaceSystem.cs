﻿using System;
using UBlockly.UI;
using UnityEngine;
using UBlockly.Util;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UBlockly.Compotent;

namespace UBlockly.Sys
{

    public partial class WorkSpaceSystem : CoreRunningSystem
    {
        internal void LoadBlock(NodeConfigAssist assitGo)
        {
            Node node = NodeFactory.CreateNode(assitGo);
            var item = InstanceLeftBarNode(node);
            leftBarHandler.AddToLeftBar(0, item);
            if (node.Block != null)
                node.Block.EventHandler.OnBeginDragDel = LeftItemBeginDrag;
        }

        public void HideOrShowSpaceBlocks(bool hideOrShow)
        {
            //physicSys.GetPhysicTransform().gameObject.SetActive(!hideOrShow);
        }

        public void ClearSpace()
        {
            //chainSys.Clear();
        }
    }
    partial class WorkSpaceSystem
    {
        long idIndex = 10000;
        ILeftBarHandler leftBarHandler;
        protected override void OnInit(params object[] args)
        {
            base.OnInit(args);
            GameObject go = UBlocklyFacade.GetIns().RootGo;
            if (go != null)
            {
                leftBarHandler = go.GetComponentInChildren<ILeftBarHandler>();
            }
        }
        void AddToWorkSpace(Node node)
        {
            node.Init();

            //事件
            if (node.Block != null)
            {
                node.Block.EventHandler.OnBeginDragDel = null;
                node.Block.EventHandler.OnDraggingDel = OnNodeDrag;
                node.Block.EventHandler.OnDropDel = OnNodeDrop;
                node.Block.EventHandler.OnClickDel = OnNodeClick;
            }

            nodeSys.AddNode(node);

            GetSys<ExtralHandlerSystem>().NodeAddToSpaceHandler?.Invoke(node, null);
        }
        Transform InstanceLeftBarNode(Node node)
        {
            RectTransform rectp = new GameObject("RectP").AddCompontentIfNoExist<RectTransform>();
            rectp.anchorMin = new Vector2(0, 1);
            rectp.anchorMax = new Vector2(0, 1);
            rectp.pivot = new Vector2(0, 1);
            if(node.Block == null)
                Debug.LogError($"null block {node.Scriptable.GetFunc().FunName}");
            rectp.sizeDelta = node.Block.GetSize();
            node.Block.transform.SetParent(rectp);
            node.Block.transform.localPosition = Vector3.zero;
            return rectp;
        }
        void LeftItemBeginDrag(PointerEventData data,BlockObject block)
        {
            var ss = block.gameObject.GetComponent<NodeConfigAssist>();
            var newnode = NodeFactory.CreateNode(ss);
            newnode.Block.transform.SetParent(block.Parent);
            newnode.Block.transform.localScale = Vector3.one;
            newnode.Block.transform.localPosition = Vector3.zero;
            newnode.Block.EventHandler.OnBeginDragDel = LeftItemBeginDrag;           

            AddToWorkSpace(block.Node.Value);
            paintingSystem.HandlerBlock(block);
        }
        void OnNodeDrag(PointerEventData data,BlockObject block)
        {
            GetSys<ExtralHandlerSystem>().NodeDragHandler?.Invoke(data, block);
        }
        void OnNodeDrop(PointerEventData data,BlockObject block)
        {
            bool isChain = block.Node.List != null;
            if (UBtil.IsRecttransAcross(block.BlockAnchor.RectTransform, leftBarHandler.rectTransform))
            {
                if (isChain)
                {
                    GetSys<ExtralHandlerSystem>().DeletaChainHandler?.Invoke(() =>
                    {
                        //var chain = chainSys.GetChain(node);
                        //if (chain != null)
                        //    chainSys.DestoryList(chain);
                    }, block);
                }
                else
                {
                    GetSys<ExtralHandlerSystem>().DeletaNodeHandler?.Invoke(() =>
                    {
                        nodeSys.RemoveNode(block.Node.Value);
                    }, block);
                }
                return;
            }
            if (!isChain)
            {
                GetSys<ExtralHandlerSystem>().NodeDropHandler?.Invoke(data, block);
            }
            else
            {
                GetSys<ExtralHandlerSystem>().ChainDropHandler?.Invoke(data, block);
            }
        }
        void OnNodeClick(PointerEventData data,BlockObject block)
        {
            GetSys<ExtralHandlerSystem>().NodeClickHandler?.Invoke(data, block);
            Debug.Log($"node {block.Node.Value.Id} be clicked");
            if (data.pointerId == -1)
            {
                string str = GetSys<TranslateSystem>().TranslateNodeToScript(block.Node.Value);
                Debug.Log("translate:" + str);
                GetSys<TranslateSystem>().RunNode(block.Node.Value);
            }
            if (data.pointerId == -2)
            {
                var list = block.Node.List;
                if (list != null)
                {
                    GetSys<TranslateSystem>().RunChain(list);
                }
            }

        }
    }
}