﻿using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UBlockly
{
    using UBlockly.UI;
    using Scriptable;

    /// <summary>
    /// 提供给外部模块的回调事件
    /// </summary>
    public class ExtralHandlerSystem : ISystem
    {
        public bool IsRunMultiMode = false;

        //转义链时
        public Action<List<Dictionary<uint, NodeScriptable>>> TranslateHandler = null;
        public Action<PointerEventData,BlockObject> NodeClickHandler = null;  //space空间下
        public Action<Node,object[]> NodeAddToSpaceHandler = null;
        public Action<PointerEventData, BlockObject> NodeDragHandler = null;
        public Action<PointerEventData, BlockObject> NodeDropHandler = null;
        public Action<PointerEventData, BlockObject> ChainDragHandler = null;
        public Action<PointerEventData, BlockObject> ChainDropHandler = null;
        public Action<Action, BlockObject> DeletaNodeHandler = null;
        public Action<Action, BlockObject> DeletaChainHandler = null;

        //运行时结束handler
        public Action<bool> RunOverCallbackHandler = null;
        
    }
}
