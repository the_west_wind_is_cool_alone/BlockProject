﻿namespace UBlockly.Sys
{
    public class CoreRunningSystem : ISystem
    {
        //public PaintingSystem paintingSys => GetSys<PaintingSystem>();
        //public PhysicSystem physicSys => GetSys<PhysicSystem>();
        //public ChainSystem chainSys => GetSys<ChainSystem>();
        public NodeSystem nodeSys => GetSys<NodeSystem>();
        public WorkSpaceSystem wspaceSys => GetSys<WorkSpaceSystem>();
        public BlockSystem blockSys => GetSys<BlockSystem>();
        public PainttingSystem paintingSystem => GetSys<PainttingSystem>();

    }
}
