﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UBlockly.Scriptable;
using UBlockly.UI;
using UBlockly.Sys;

namespace UBlockly.Util
{
    
    public static class AutoFilterScreen
    {
        static Vector2 AutoScale;
        static Vector2 constResolution = new Vector2(1080, 1920);

        public static void Init(Vector2 resolution)
        {
            AutoScale = new Vector2(resolution.x / constResolution.x, resolution.y / constResolution.y);
        }

        public static Vector2 GetResizeScale()
        {
            return AutoScale;
        }

        public static float GetXSpace(float x)
        {
            return x * AutoScale.x;
        }

        public static float GetYSpace(float y)
        {
            return y * AutoScale.y;
        }

        public static Vector2 GetVector(Vector2 v)
        {
            return new Vector2(v.x * AutoScale.x, v.y * AutoScale.y);
        }
        public static Vector3 GetVector(Vector3 v)
        {
            return GetVector((Vector2)v);
        }
    }

    public class UBlocklyFacade
    {
        public GameObject RootGo;
        private Dictionary<Type, ISystem> _dic = new Dictionary<Type, ISystem>();
        private GameObject canvas;
        public static ExtralHandlerSystem ExtralHandler => GetIns().GetSys<ExtralHandlerSystem>();
        static UBlocklyFacade _ins;
        public static UBlocklyFacade GetIns()
        {
            if (_ins == null)
                _ins = new UBlocklyFacade();
            return _ins;
        }
        private UBlocklyFacade()
        { 
            _dic.Add(typeof(NodeSystem), new NodeSystem());
            _dic.Add(typeof(WorkSpaceSystem), new WorkSpaceSystem());
            _dic.Add(typeof(BlockSystem),new BlockSystem());
            _dic.Add(typeof(PainttingSystem), new PainttingSystem());
            _dic.Add(typeof(ExtralHandlerSystem), new ExtralHandlerSystem());
            _dic.Add(typeof(TranslateSystem), new TranslateSystem());
        }

        public void InitFrameWork(IBlockApi api)
        {
            ILTranslate.LoadApi(api);
        }

        /// <summary>
        /// 加载界面
        /// </summary>
        /// <param name="go"></param>
        public void LoadUBlockly(UnityEngine.GameObject go)
        {
            this.RootGo = go;
            go.name = "UBlockly";

            AutoFilterScreen.Init(new Vector2(Screen.width, Screen.height));
            go.transform.localScale = AutoFilterScreen.GetResizeScale();
            go.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            foreach (var kvp in _dic)
            {
                System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
                System.Reflection.MethodInfo method = kvp.Value.GetType().GetMethod("OnInit", flags);
                method.Invoke(kvp.Value, new object[] { null });
            }
        }

        public UnityEngine.Transform Find(string name)
        {
            return RootGo.transform.Find(name);
        }

        public void InitBlocks(List<NodeConfigAssist> blocks)
        {
            foreach (var assit in blocks)
            {
                GetSys<WorkSpaceSystem>().LoadBlock(assit);
            }
        }

        public void CleanWorkspace(System.Action callback)
        {
            //GetSys<WorkSpaceSystem>().CleanWorkSpace(callback);
        }

        public TSys GetSys<TSys>() where TSys : ISystem
        {
            ISystem value = null;
            if (_dic.TryGetValue(typeof(TSys), out value))
            {
                return value as TSys;
            }
            return null;
        }
    }
}
