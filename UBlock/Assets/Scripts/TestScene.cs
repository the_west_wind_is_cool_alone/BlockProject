﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LinkedListNode<TestNode> tnode = new LinkedListNode<TestNode>(new TestNode(11));
        Debug.Log($"list == null ? {tnode.List == null}");
        var list = new LinkedList<TestNode>();
        list.AddFirst(tnode);
        for (int i = 0; i < 10; i++)
        {
            list.AddLast(new TestNode(12 + i));
        }

        //list.Remove()
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class TestNode
{
    public int x;
    public TestNode(int dd)
    {
        x = dd;
    }
}
