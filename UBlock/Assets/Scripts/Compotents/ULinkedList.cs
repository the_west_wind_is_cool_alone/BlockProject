﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBlockly.UI;

namespace UBlockly.Compotent
{

  /// <summary>
  /// 当长度发生改变时，有事件抛出
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public sealed class ULinkedList<T> : LinkedList<T> where T: IChangeNotify  
  {
    public new LinkedListNode<T> AddAfter(LinkedListNode<T> node, T value)
    {
      var result = base.AddAfter(node, value);
      DoNotify();
      return result;
    }

    public new void AddAfter(LinkedListNode<T> node, LinkedListNode<T> newNode)
    {
      base.AddAfter(node, newNode);
      DoNotify();
    }

    public new LinkedListNode<T> AddBefore(LinkedListNode<T> node, T value)
    {
      var result = base.AddBefore(node, value);
      DoNotify();
      return result;
    }

    public new void AddBefore(LinkedListNode<T> node, LinkedListNode<T> newNode)
    {
      base.AddBefore(node, newNode);
      DoNotify();
    }

    public new LinkedListNode<T> AddFirst(T value)
    {
      var result = base.AddFirst(value);
      DoNotify();
      return result;
    }

    public new void AddFirst(LinkedListNode<T> node)
    {
      base.AddFirst(node);
      DoNotify();
    }

    public new LinkedListNode<T> AddLast(T value)
    {
      var result = AddLast(value);
      DoNotify();
      return result;
    }

    public new void AddLast(LinkedListNode<T> node)
    {
      base.AddLast(node);
      DoNotify();
    }

    public new void Clear()
    {
      base.Clear();
      DoNotify();
    }

    public new bool Remove(T value)
    {
      var result = base.Remove(value);
      DoNotify();
      return result;
    }

    public new void Remove(LinkedListNode<T> node)
    {
      base.Remove(node);
      DoNotify();
    }

    public new void RemoveFirst()
    {
      base.RemoveFirst();
      DoNotify();
    }

    public new void RemoveLast()
    {
      base.RemoveLast();
      DoNotify();
    }

    public void MuteNotify()
    {
      isMute = true;
    }

    public void OpenNoify()
    {
      isMute = false;
    }

    public void DoNotify()
    {
      if (!isMute)
      {
        First.Value.OnChangeNotify();
        //OnULinkedListChangedNotify?.Invoke();
      }
    }


    private bool isMute = false;
    //public System.Action OnULinkedListChangedNotify;
  }
  

}

