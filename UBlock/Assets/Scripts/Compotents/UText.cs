﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UBlockly.Compotent
{
    [RequireComponent(typeof(BlockAnchor))]
    public class UText : Text
    {
        BlockAnchor UTrans => GetComponent<BlockAnchor>();
        public override string text { get => base.text; set => SetText(value); }

        void SetText(string text)
        {
            //float origLen = UTrans.GetSize().x;
            //float len = Common.GetFontLen(this.font, this.fontSize, text);
            //len = UTrans.MinSize.x > len ? UTrans.MinSize.x : len;
            ////UTrans.ChangeSize(new Vector2(len - origLen, 0));
            base.text = text;
        }
    }
}
