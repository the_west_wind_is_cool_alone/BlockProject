﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UBlockly.Compotent
{

    [AddComponentMenu("BlocklyUI/UInput Field")]
    [RequireComponent(typeof(BlockAnchor))]
    public class UInputField : InputField
    {

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);
            //float origLen = UTrans.GetSize().x;
            //float len = Common.GetFontLen(this.m_TextComponent.font, this.m_TextComponent.fontSize, text);
            //len = UTrans.MinSize.x > len ? UTrans.MinSize.x : len;
        }
    }
}

