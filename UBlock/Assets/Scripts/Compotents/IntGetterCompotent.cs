﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    [RequireComponent(typeof(UInputField))]
    public class IntGetterCompotent : ParamGetterCompotent
    {
        [SerializeField]
        UInputField uInput;

        void Awake()
        {
            uInput = GetComponent<UInputField>();
            uInput.contentType = UnityEngine.UI.InputField.ContentType.IntegerNumber;
        }

        protected override object InputGetter()
        {
            return int.Parse(uInput.text);
        }
    }
}
