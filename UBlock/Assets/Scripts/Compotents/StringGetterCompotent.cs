﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    [RequireComponent(typeof(UInputField))]
    public class StringGetterCompotent : ParamGetterCompotent
    {
        [SerializeField]
        UInputField uInput;
        private void Awake()
        {
            uInput = GetComponent<UInputField>();
            uInput.contentType = UnityEngine.UI.InputField.ContentType.Standard;
        }
        
        protected override object InputGetter()
        {
            return uInput.text;
        }
    }
}
