﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    public interface ILeftBarHandler
    {
        RectTransform rectTransform { get; }
        void AddToLeftBar(int classifly, Transform trans);
        void InitClassifly(string[] tags);
        
    }
    public class ULeftBarHandler : MonoBehaviour,ILeftBarHandler
    {
        [SerializeField] private Transform leftBarTransform;
        public RectTransform rectTransform => GetComponent<RectTransform>();
        public void AddToLeftBar(int classifly, Transform trans)
        {
            trans.SetParent(leftBarTransform);
            trans.localScale = Vector3.one;
        }
        public void InitClassifly(string[] tags)
        { }
    }
}
