﻿namespace UBlockly.Compotent
{
    /// <summary>
    /// 获取输入值
    /// </summary>
    public interface IInputGetter
    {
        object GetterValue();
    }
}
