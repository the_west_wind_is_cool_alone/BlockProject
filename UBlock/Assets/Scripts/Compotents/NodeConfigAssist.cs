﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Security.Cryptography;

namespace UBlockly.UI
{
    using Scriptable;

    [ExecuteInEditMode]
    public class NodeConfigAssist : MonoBehaviour
    {
        [SerializeField]
        public string FunctionName;
        [SerializeField]
        public ILFuncType FuncType;
        [SerializeField]
        public string Desc;
        [SerializeField]
        public Transform ParamTransform;
        [SerializeField]
        public Transform[] Slots;
        [SerializeField]
        public Transform[] Packs;
      
        public ILFuncType GetFuncType()
        {
            return FuncType;
        }
        public string GetDesc()
        {
            return Desc;
        }
        public Transform[] CreatePacks(long id)
        {
            return Packs;
        }
        public NSlotPair[] CreateSlotPiars(long id)
        {
            if (Slots == null || Slots.Length == 0 || Slots.Length % 2 != 0)
                return null;
            NSlotPair[] pairs = new NSlotPair[Slots.Length / 2];
            for (int i = 0; i < Slots.Length; i += 2)
            {
                var pair = new NSlotPair(id, Slots[i], Slots[i + 1]);
                pairs[i / 2] = pair;
            }
            return pairs;
        }
        public Transform GetParamTransform()
        {
            return ParamTransform;
        }
    }
}
