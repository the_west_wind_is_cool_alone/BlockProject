﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    public class BoolGetterCompotent : ParamGetterCompotent
    {
        protected override object InputGetter()
        {
            return false;
            //throw new System.NotImplementedException();
        }
    }
}
