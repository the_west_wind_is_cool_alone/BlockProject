﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    [RequireComponent(typeof(UDropdown))]
    public class DropdownGetterCompotent : ParamGetterCompotent
    {
        UDropdown dropdown;

        private void Awake()
        {
            dropdown = GetComponent<UDropdown>();
        }

        protected override object InputGetter()
        {
            return dropdown.value;
        }
    }
}
