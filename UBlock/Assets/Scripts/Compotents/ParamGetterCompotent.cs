﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBlockly.Compotent
{
    public abstract class ParamGetterCompotent : MonoBehaviour,IInputGetter
    {
        public object GetterValue()
        {
            return InputGetter();
        }

        protected abstract object InputGetter();
    }
}
