﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UBlockly.UI;
using UnityEngine.EventSystems;
using System;

namespace UBlockly.Compotent
{

    [RequireComponent(typeof(BlockAnchor))]
    public class UDropdown : Dropdown
    {
        Vector2 origSize;
        public Action<BaseEventData> OnSelectDel;
        protected override void Awake()
        {
            base.Awake();
            origSize = GetComponent<RectTransform>().sizeDelta;
            onValueChanged.AddListener((str) =>
            {
                float len = Common.GetFontLen(this.captionText.font, this.captionText.fontSize, this.captionText.text);
                len += 20; //输入框
            len = this.origSize.x > len ? this.origSize.x : len;
                GetComponent<RectTransform>().Resize(len);
            });
        }
     
        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
            this.OnSelectDel?.Invoke(eventData);
        }
    }
}

